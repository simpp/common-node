"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  validate: true,
  bodyObjectId: true,
  queryObjectId: true,
  paramObjectId: true,
  bodyArrayObjectIds: true,
  queryArrayObjectIds: true,
  queryInValues: true
};
exports.validate = exports.queryObjectId = exports.queryInValues = exports.queryArrayObjectIds = exports.paramObjectId = exports.bodyObjectId = exports.bodyArrayObjectIds = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _mongodb = require("mongodb");

var _expressValidator = require("express-validator");

Object.keys(_expressValidator).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _expressValidator[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _expressValidator[key];
    }
  });
});

var _error = require("./error");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var validate = validators => {
  return [..._lodash.default.castArray(validators), function validate(req, res, next) {
    var errorResult = (0, _expressValidator.validationResult)(req).array();

    if (_lodash.default.isEmpty(errorResult)) {
      return next();
    }

    var errorPair = _lodash.default.map(errorResult, error => {
      var prop = _lodash.default.get(error, 'param');

      var msg = _lodash.default.get(error, 'msg');

      var first = _lodash.default.get(msg, 'key', prop);

      var second = _lodash.default.get(msg, 'error', msg);

      return [_lodash.default.template(first)(error), _lodash.default.template(second)(error)];
    });

    throw new _error.ClientError(_lodash.default.fromPairs(errorPair));
  }];
};

exports.validate = validate;

var bodyObjectId = (field, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.body)(field).custom(function bodyObjectId(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    if (_mongodb.ObjectId.isValid(value)) {
      return true;
    }

    throw new Error('[ObjectId].invalid');
  });
};

exports.bodyObjectId = bodyObjectId;

var queryObjectId = (field, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.query)(field).custom(function queryObjectId(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    if (_mongodb.ObjectId.isValid(value)) {
      return true;
    }

    throw new Error('[ObjectId].invalid');
  });
};

exports.queryObjectId = queryObjectId;

var paramObjectId = (field, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.param)(field).custom(function paramObjectId(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    if (_mongodb.ObjectId.isValid(value)) {
      return true;
    }

    throw new Error('[ObjectId].invalid');
  });
};

exports.paramObjectId = paramObjectId;

var bodyArrayObjectIds = (field, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.body)(field).custom(function bodyArrayObjectIds(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    var ids = _lodash.default.castArray(value);

    if (_lodash.default.every(ids, _mongodb.ObjectId.isValid)) {
      return true;
    }

    throw new Error('[ObjectId].invalid');
  });
};

exports.bodyArrayObjectIds = bodyArrayObjectIds;

var queryArrayObjectIds = (field, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.query)(field).custom(function queryArrayObjectIds(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    var rawIds = _lodash.default.isArray(value) ? value : _lodash.default.split(value, ',');

    var ids = _lodash.default.map(rawIds, id => _lodash.default.trim(id, '"'));

    if (_lodash.default.every(ids, id => _mongodb.ObjectId.isValid(id))) {
      return true;
    }

    throw new Error('[ObjectId].invalid');
  });
};

exports.queryArrayObjectIds = queryArrayObjectIds;

var queryInValues = (field, values, options) => {
  var ignoreIfEmpty = _lodash.default.get(options, 'ignoreIfEmpty', true);

  var isRequired = _lodash.default.get(options, 'isRequired', false);

  return (0, _expressValidator.query)(field).custom(function queryObjectId(value) {
    if (_lodash.default.isEmpty(value) && isRequired) {
      throw new Error('required');
    }

    if (_lodash.default.isEmpty(value) && ignoreIfEmpty) {
      return true;
    }

    if (_lodash.default.includes(values, value)) {
      return true;
    }

    throw new Error('invalid');
  });
};

exports.queryInValues = queryInValues;