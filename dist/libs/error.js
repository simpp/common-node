"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clientErrorHandler = exports.ForbiddenError = exports.ClientError = exports.AuthorizationError = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _httpStatusCodes = require("http-status-codes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ClientError extends Error {
  constructor(detail, statusCode) {
    super(JSON.stringify(detail));

    _defineProperty(this, "statusCode", _httpStatusCodes.StatusCodes.BAD_REQUEST);

    _defineProperty(this, "errorCodes", ['BAD_REQUEST']);

    this.detail = detail;
    this.statusCode = statusCode || this.statusCode;
  }

  withCodes(codes) {
    this.errorCodes = _lodash.default.castArray(codes || []);
    return this;
  }

}

exports.ClientError = ClientError;

class AuthorizationError extends Error {
  constructor() {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'not allowed';
    super(message);

    _defineProperty(this, "statusCode", _httpStatusCodes.StatusCodes.UNAUTHORIZED);

    _defineProperty(this, "errorCodes", ['UNAUTHORIZED']);

    this.detail = {
      message
    };
  }

  withCodes(codes) {
    this.errorCodes = _lodash.default.castArray(codes || []);
    return this;
  }

}

exports.AuthorizationError = AuthorizationError;

class ForbiddenError extends Error {
  constructor() {
    var message = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'You are not allowed to do this action!';
    super(message);

    _defineProperty(this, "statusCode", _httpStatusCodes.StatusCodes.FORBIDDEN);

    _defineProperty(this, "errorCodes", ['FORBIDDEN']);

    this.detail = {
      message
    };
  }

  withCodes(codes) {
    this.errorCodes = _lodash.default.castArray(codes || []);
    return this;
  }

}

exports.ForbiddenError = ForbiddenError;

var clientErrorHandler = (error, req, res, next) => {
  if (error instanceof ClientError) {
    res.fail(_httpStatusCodes.StatusCodes.BAD_REQUEST, error.detail, error.errorCodes);
    return;
  }

  if (error instanceof AuthorizationError) {
    res.fail(_httpStatusCodes.StatusCodes.UNAUTHORIZED, error.detail, error.errorCodes);
    return;
  }

  if (error instanceof ForbiddenError) {
    res.fail(_httpStatusCodes.StatusCodes.FORBIDDEN, error.detail, error.errorCodes);
    return;
  }

  next(error);
};

exports.clientErrorHandler = clientErrorHandler;