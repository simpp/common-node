"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_lodash.default.__proto__.isSEqual = (a, b) => _lodash.default.toString(a) === _lodash.default.toString(b);

var _default = _lodash.default;
exports.default = _default;