"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useTimestamp = exports.useString = exports.useObjectId = exports.useNumber = exports.useInt = exports.useEnum = exports.useBoolean = exports.useBigNumber = exports.useArrayEnum = exports.useArray = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _bignumber = _interopRequireDefault(require("bignumber.js"));

var _requestHandler = require("./request-handler");

var _error = require("./error");

var _mongodb = require("./mongodb");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var useString = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useString] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useString] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.trim(_lodash.default.get(req[source], fieldName));

    if (!value) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    ctx.setData(alias || fieldName, value);
  });
};

exports.useString = useString;

var useObjectId = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useObjectId] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useObjectId] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.trim(_lodash.default.get(req[source], fieldName));

    if (!value) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    if (!(0, _mongodb.isObjectId)(value)) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    ctx.setData(alias || fieldName, value);
  });
};

exports.useObjectId = useObjectId;

var useTimestamp = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useTimestamp] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useTimestamp] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var numberValue = Number(value);

    var parsed = _moment.default.utc(numberValue || value);

    if (!parsed.isValid()) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    ctx.setData(alias || fieldName, parsed.valueOf());
  });
};

exports.useTimestamp = useTimestamp;

var useInt = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useInt] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useInt] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var numberValue = Number(value);

    if (Number.isInteger(numberValue)) {
      ctx.setData(alias || fieldName, numberValue);
    } else {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      } else {
        ctx.setData(alias || fieldName, defaultValue);
      }
    }
  });
};

exports.useInt = useInt;

var useNumber = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  var minValue = _lodash.default.get(options, 'min');

  var maxValue = _lodash.default.get(options, 'max');

  if (!fieldName) {
    throw new Error('[useNumber] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useNumber] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var numberValue = Number(value);

    if (Number.isNaN(numberValue)) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    if (!_lodash.default.isNil(maxValue)) {
      numberValue = Math.max(numberValue, minValue);
    }

    if (!_lodash.default.isNil(maxValue)) {
      numberValue = Math.min(numberValue, maxValue);
    }

    ctx.setData(alias || fieldName, numberValue);
  });
};

exports.useNumber = useNumber;

var useBigNumber = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  var minValue = _lodash.default.get(options, 'min');

  var maxValue = _lodash.default.get(options, 'max');

  if (!fieldName) {
    throw new Error('[useNumber] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useNumber] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var numberValue = (0, _bignumber.default)(value);

    if (numberValue.isNaN()) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    if (_lodash.default.isNil(minValue)) {
      numberValue = _bignumber.default.max(numberValue, minValue);
    }

    if (_lodash.default.isNil(maxValue)) {
      numberValue = _bignumber.default.min(numberValue, maxValue);
    }

    ctx.setData(alias || fieldName, numberValue.toString());
    return;
  });
};

exports.useBigNumber = useBigNumber;

var useBoolean = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useBoolean] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useBoolean] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var boolValue = typeof value === 'string' ? value === 'true' : Boolean(value);
    ctx.setData(alias || fieldName, boolValue);
  });
};

exports.useBoolean = useBoolean;

var useEnum = (fieldName, enumData, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useEnum] fieldName value must be provided');
  }

  if (!enumData) {
    throw new Error('[useEnum] enumData value must be provided');
  }

  if (!source) {
    throw new Error('[useEnum] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(_lodash.default.trim(value))) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var found = _lodash.default.find(enumData, enumValue => enumValue === value);

    if (found) {
      ctx.setData(alias || fieldName, value);
    } else {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'invalid'
        });
      } else {
        ctx.setData(alias || fieldName, defaultValue);
      }
    }
  });
};

exports.useEnum = useEnum;

var useArrayEnum = (fieldName, enumData, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useArrayEnum] fieldName value must be provided');
  }

  if (!enumData) {
    throw new Error('[useArrayEnum] enumData value must be provided');
  }

  if (!source) {
    throw new Error('[useArrayEnum] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var arrayValues = _lodash.default.castArray(_lodash.default.get(req[source], fieldName) || []);

    if (_lodash.default.isEmpty(arrayValues)) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    var notExists = findValue => !_lodash.default.find(enumData, enumValue => enumValue === findValue);

    if (_lodash.default.some(arrayValues, notExists)) {
      throw new _error.ClientError({
        [fieldName]: 'invalid'
      });
    }

    ctx.setData(alias || fieldName, arrayValues);
  });
};

exports.useArrayEnum = useArrayEnum;

var useArray = (fieldName, options) => {
  var isRequired = _lodash.default.get(options, 'isRequired', false);

  var alias = _lodash.default.get(options, 'alias', null);

  var source = _lodash.default.get(options, 'source', null);

  var defaultValue = _lodash.default.get(options, 'default');

  if (!fieldName) {
    throw new Error('[useArray] fieldName value must be provided');
  }

  if (!source) {
    throw new Error('[useArray] source value must be provided');
  }

  return (0, _requestHandler.tryNext)((req, res, next, ctx) => {
    var value = _lodash.default.get(req[source], fieldName);

    if (_lodash.default.isEmpty(value)) {
      if (isRequired) {
        throw new _error.ClientError({
          [fieldName]: 'required'
        });
      }

      ctx.setData(alias || fieldName, defaultValue);
      return;
    }

    ctx.setData(alias || fieldName, _lodash.default.castArray(value || []));
  });
};

exports.useArray = useArray;