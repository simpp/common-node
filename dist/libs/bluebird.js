"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = require("bluebird");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

_bluebird.Promise.__proto__.allOf = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (arr, it) {
    for (var i = 0; i < arr.length; i++) {
      var res = it;

      if (_lodash.default.isFunction(it)) {
        res = yield it(arr[i]);
      }

      if (!res) {
        return false;
      }
    }

    return true;
  });

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

_bluebird.Promise.__proto__.anyOf = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (arr, it) {
    for (var i = 0; i < arr.length; i++) {
      var res = it;

      if (_lodash.default.isFunction(it)) {
        res = yield it(arr[i]);
      }

      if (res) {
        return true;
      }
    }

    return false;
  });

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var _default = _bluebird.Promise;
exports.default = _default;