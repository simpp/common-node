"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sort = exports.hash = exports.assignValueOnce = exports.assignObjOnce = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _crypto = _interopRequireDefault(require("crypto"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var assignValueOnce = (obj, prop, value) => {
  obj[prop] = value;
  Object.defineProperty(obj, prop, {
    writable: false
  });
  return obj;
};

exports.assignValueOnce = assignValueOnce;

var assignObjOnce = (obj, src) => {
  _lodash.default.each(src, (value, prop) => assignValueOnce(obj, prop, value));

  return obj;
};

exports.assignObjOnce = assignObjOnce;

var hash = obj => {
  var checkSum = _crypto.default.createHash('md5').update(JSON.stringify(obj), 'utf8').digest('hex');

  return checkSum;
};

exports.hash = hash;

var sort = input => {
  if (typeof input === 'object') {
    if (input instanceof Array) {
      return input.sort((a, b) => a > b ? 1 : -1);
    }

    var nestedObject = _lodash.default.mapValues(input, value => sort(value));

    var keys = _lodash.default.keys(input).sort((a, b) => a > b ? 1 : -1);

    return _lodash.default.reduce(keys, (obj, key) => _objectSpread(_objectSpread({}, obj), {}, {
      [key]: nestedObject[key]
    }), {});
  }

  return input;
};

exports.sort = sort;