"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parse = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _secret = require("./secret");

var _file = require("./file");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var format = (data, formatters) => {
  return _lodash.default.reduce(formatters, (result, formatter) => eval(formatter)(result), data);
};

var parseKeyPath = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (configKeyPath, configDefault, nodePipe) {
    var valueFromFile = yield (0, _secret.read)(configKeyPath);
    return format(valueFromFile, nodePipe);
  });

  return function parseKeyPath(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var parseEnvVar = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (configEnvVar, configDefault, nodePipe) {
    var valueFromEnv = _lodash.default.get(process.env, configEnvVar, configDefault);

    return format(valueFromEnv, nodePipe);
  });

  return function parseEnvVar(_x4, _x5, _x6) {
    return _ref2.apply(this, arguments);
  };
}();

var parseJsonPath = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (configJsonPath, configJsonProp, configDefault, nodePipe) {
    var jsonFromFile = yield (0, _file.readJsonAsync)(configJsonPath);
    var valueFromJson = configJsonProp ? _lodash.default.get(jsonFromFile, configJsonProp, configDefault) : jsonFromFile;
    return format(valueFromJson, nodePipe);
  });

  return function parseJsonPath(_x7, _x8, _x9, _x10) {
    return _ref3.apply(this, arguments);
  };
}();

var parseRoot = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator(function* (config) {
    if (typeof config !== 'object') {
      return config;
    }

    var configKeyPath = _lodash.default.get(config, ['__parse+keyPath']);

    var configJsonPath = _lodash.default.get(config, ['__parse+jsonPath']);

    var configJsonProp = _lodash.default.get(config, ['__parse+jsonProp']);

    var configEnvVar = _lodash.default.get(config, ['__parse+envVar']);

    var configDefault = _lodash.default.get(config, ['__parse+default']);

    var nodePipe = _lodash.default.castArray(_lodash.default.get(config, ['__parse+nodePipe']) || []);

    if (typeof configEnvVar === 'string') {
      return yield parseEnvVar(configEnvVar, configDefault, nodePipe);
    } else if (typeof configKeyPath === 'string') {
      return yield parseKeyPath(configKeyPath, configDefault, nodePipe);
    } else if (typeof configJsonPath === 'string') {
      return yield parseJsonPath(configJsonPath, configJsonProp, configDefault, nodePipe);
    }

    return config;
  });

  return function parseRoot(_x11) {
    return _ref4.apply(this, arguments);
  };
}();

var parse = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator(function* (input) {
    var config = yield parseRoot(input);

    if (typeof config !== 'object') {
      return config;
    }

    var props = _lodash.default.keys(config);

    var result = yield _bluebird.default.reduce(props, /*#__PURE__*/function () {
      var _ref6 = _asyncToGenerator(function* (parsedResult, prop) {
        var configValue = _lodash.default.get(config, prop);

        var configKeyPath = _lodash.default.get(config, [prop, '__parse+keyPath']);

        var configJsonPath = _lodash.default.get(config, [prop, '__parse+jsonPath']);

        var configJsonProp = _lodash.default.get(config, [prop, '__parse+jsonProp']);

        var configEnvVar = _lodash.default.get(config, [prop, '__parse+envVar']);

        var configDefault = _lodash.default.get(config, [prop, '__parse+default']);

        var nodePipe = _lodash.default.castArray(_lodash.default.get(config, [prop, '__parse+nodePipe']) || []);

        parsedResult[prop] = configValue;

        if (typeof configValue === 'string') {
          parsedResult[prop] = configValue;
        } else if (typeof configEnvVar === 'string') {
          parsedResult[prop] = yield parseEnvVar(configEnvVar, configDefault, nodePipe);
        } else if (typeof configKeyPath === 'string') {
          parsedResult[prop] = yield parseKeyPath(configKeyPath, configDefault, nodePipe);
        } else if (typeof configJsonPath === 'string') {
          parsedResult[prop] = yield parseJsonPath(configJsonPath, configJsonProp, configDefault, nodePipe);
        } else if (typeof configValue === 'object') {
          if (Array.isArray(configValue)) {
            parsedResult[prop] = yield _bluebird.default.map(configValue, parse);
          } else {
            parsedResult[prop] = yield parse(configValue);
          }
        } else {
          parsedResult[prop] = configValue;
        }

        return parsedResult;
      });

      return function (_x13, _x14) {
        return _ref6.apply(this, arguments);
      };
    }(), {});
    return result;
  });

  return function parse(_x12) {
    return _ref5.apply(this, arguments);
  };
}();

exports.parse = parse;