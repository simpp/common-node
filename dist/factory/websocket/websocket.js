"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = exports.createInstance = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _ltws = _interopRequireDefault(require("ltws"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var init = (ctx, options) => {
  var websocket = _ltws.default.connect(options.origin, options.config);

  return {
    websocket
  };
};

exports.init = init;

var createInstance = () => {
  var websocket = _ltws.default.createInstance();

  return {
    websocket
  };
};

exports.createInstance = createInstance;