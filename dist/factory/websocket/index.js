"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _websocket = require("./websocket");

var _default = {
  init: _websocket.init,
  createInstance: _websocket.createInstance
};
exports.default = _default;