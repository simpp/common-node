"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _agenda = require("./agenda");

var _default = {
  init: _agenda.init,
  fromMongodb: _agenda.fromMongodb
};
exports.default = _default;