"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _socket = require("./socket.io-redis");

var _default = {
  init: _socket.init
};
exports.default = _default;