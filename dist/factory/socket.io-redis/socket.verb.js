"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bind = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _socket = require("./socket.io-redis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bind = socket => {
  var verbs = [];

  var socketLib = _lodash.default.fromPairs(_lodash.default.map(verbs, verb => [verb, socket[verb].bind(socket)]));

  socketLib.sendToRoom = (paths, data) => {
    return socket.to((0, _socket.toRoomId)(paths)).send(data);
  };

  socketLib.sendToManyRoom = (paths, data) => {
    return (0, _socket.goToRooms)(socket, paths).send(data);
  };

  socketLib.emitTo = (paths, event, data) => {
    return socket.to((0, _socket.toRoomId)(paths)).emit(event, data);
  };

  socketLib.emitToMany = (paths, event, data) => {
    return (0, _socket.goToRooms)(socket, paths).emit(event, data);
  };

  socketLib.with = nsp => bind(socket.of(nsp));

  socketLib.withMany = function () {
    for (var _len = arguments.length, nsp = new Array(_len), _key = 0; _key < _len; _key++) {
      nsp[_key] = arguments[_key];
    }

    var nsps = _lodash.default.flatten(nsp);

    return {
      sendToRoom: function sendToRoom() {
        for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments[_key2];
        }

        _lodash.default.each(nsps, nsp => bind(socket.of(nsp)).sendToRoom(...args));
      },
      sendToManyRoom: function sendToManyRoom() {
        for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
          args[_key3] = arguments[_key3];
        }

        _lodash.default.each(nsps, nsp => bind(socket.of(nsp)).sendToManyRoom(...args));
      },
      emitTo: function emitTo() {
        for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
          args[_key4] = arguments[_key4];
        }

        _lodash.default.each(nsps, nsp => bind(socket.of(nsp)).emitTo(...args));
      },
      emitToMany: function emitToMany() {
        for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
          args[_key5] = arguments[_key5];
        }

        _lodash.default.each(nsps, nsp => bind(socket.of(nsp)).emitToMany(...args));
      }
    };
  };

  return socketLib;
};

exports.bind = bind;