"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = void 0;

var _util = _interopRequireDefault(require("util"));

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _moment = _interopRequireDefault(require("moment"));

var _winston = require("winston");

var _transport = _interopRequireDefault(require("./transport"));

var _excluded = ["type"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var transformData = data => {
  var formatters = _lodash.default.map(data, data => {
    if (data instanceof Error) {
      return _util.default.formatWithOptions({
        colors: true,
        depth: 5,
        showHidden: true
      }, '%O', data.stack);
    }

    switch (typeof data) {
      case 'string':
        return _util.default.formatWithOptions({
          colors: true
        }, '%s', data);

      case 'number':
        return _util.default.formatWithOptions({
          colors: true
        }, '%d', data);

      default:
        return _util.default.formatWithOptions({
          colors: true,
          depth: 5
        }, '%O', data);
    }
  });

  return _lodash.default.join(formatters, ' ');
};

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx) {
    var {
      config: {
        logger: {
          transports,
          level
        }
      }
    } = ctx;
    var {
      combine,
      printf,
      simple
    } = _winston.format;
    var finalFormat = printf(_ref2 => {
      var {
        message,
        level,
        [Symbol.for('splat')]: optionalData
      } = _ref2;

      var data = _lodash.default.filter(_lodash.default.flatten([message, optionalData]), item => !_lodash.default.isNil(item));

      var transformedData = transformData(data);
      return "".concat(_moment.default.utc().toISOString(), " ").concat(level, " ").concat(transformedData);
    });
    var defaultFormats = [simple(), finalFormat];
    var logger = (0, _winston.createLogger)({
      level,
      format: combine(...defaultFormats),
      transports: yield _bluebird.default.map(transports, _ref3 => {
        var {
          type
        } = _ref3,
            opts = _objectWithoutProperties(_ref3, _excluded);

        return (0, _transport.default)(type, opts, defaultFormats);
      })
    });
    return {
      logger
    };
  });

  return function init(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.init = init;