"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.send = void 0;

var send = (ctx, transporter, body) => new Promise((resolve, reject) => {
  transporter.sendMail(body, (err, result) => err ? reject(err) : resolve(result));
});

exports.send = send;