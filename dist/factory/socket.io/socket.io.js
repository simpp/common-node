"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toRoomIds = exports.toRoomId = exports.init = exports.goToRooms = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _socket = _interopRequireDefault(require("socket.io"));

var Verb = _interopRequireWildcard(require("./socket.verb"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, options) {
    var {
      instances: {
        server
      }
    } = ctx;
    var socket = (0, _socket.default)(server, options);

    _lodash.default.assign(socket, Verb.bind(socket), {
      toRoomId,
      goToRooms
    });

    return {
      socket
    };
  });

  return function init(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.init = init;

var toRoomId = paths => _lodash.default.join(_lodash.default.flattenDeep(paths), ':');

exports.toRoomId = toRoomId;

var toRoomIds = paths => _lodash.default.map(paths, toRoomId);

exports.toRoomIds = toRoomIds;

var goToRooms = (socket, rooms) => {
  return _lodash.default.reduce(rooms, (socket, room) => socket.to(toRoomId(room)), socket);
};

exports.goToRooms = goToRooms;