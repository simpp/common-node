"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bind = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _redis = require("./redis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bind = redis => {
  var prefix = _lodash.default.get(redis, 'options.keyPrefix', '');

  var verbs = [];

  var redisLib = _lodash.default.fromPairs(_lodash.default.map(verbs, verb => [verb, redis[verb].bind(redis)]));

  redisLib.toKey = paths => "".concat(prefix).concat((0, _redis.toKey)(paths));

  redisLib.deKey = key => key.substr(_lodash.default.size(prefix));

  return redisLib;
};

exports.bind = bind;