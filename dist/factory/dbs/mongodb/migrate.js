"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.migrate = void 0;

var _path = _interopRequireDefault(require("path"));

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var migrate = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, dbs, dbName, scriptName) {
    var {
      instances: {
        logger
      }
    } = ctx;

    var dbMigrations = require(_path.default.join(global.MAIN_DIR, 'models/migrate')).default;

    var db = dbs[dbName];

    var migrations = _lodash.default.filter(_lodash.default.flatMap(dbMigrations[dbName]), script => !scriptName || script.name === scriptName);

    if (!db) {
      throw new Error("No db for [".concat(dbName, "]"));
    }

    logger.info("Starting...");
    yield _bluebird.default.each(migrations, /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator(function* (migration) {
        logger.info("Run migrate on [".concat(migration.name, "]..."));

        var migrationSteps = _lodash.default.flatten(_lodash.default.castArray(migration.steps || migration.step || []));

        yield _bluebird.default.reduce(migrationSteps, /*#__PURE__*/function () {
          var _ref4 = _asyncToGenerator(function* (_ref3, step, stepIndex, stepCount) {
            var {
              results,
              prev
            } = _ref3;
            logger.info("[".concat(step.name || stepIndex + 1, "] running..."));
            var curResult = null;

            try {
              curResult = yield step(ctx, {
                db,
                dbs,
                dbName,
                results,
                prev
              });
            } catch (err) {
              logger.error("[".concat(step.name || stepIndex + 1, "] failed!"), err);
              throw err;
            }

            logger.info("[".concat(step.name || stepIndex + 1, "] ").concat(stepIndex + 1, "/").concat(stepCount, " done."));
            return {
              prev: curResult,
              results: [...results, curResult]
            };
          });

          return function (_x6, _x7, _x8, _x9) {
            return _ref4.apply(this, arguments);
          };
        }(), {
          results: [],
          previous: null
        });
        logger.info("Run migrate on [".concat(migration.name, "], done."));
      });

      return function (_x5) {
        return _ref2.apply(this, arguments);
      };
    }());
    logger.info("migrate on [".concat(dbName, "] is done"));
  });

  return function migrate(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();

exports.migrate = migrate;