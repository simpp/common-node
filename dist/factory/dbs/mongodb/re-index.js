"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reIndex = void 0;

var _path = _interopRequireDefault(require("path"));

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _excluded = ["key", "name"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var ROTATE_MODEL_TYPES = ['moment', 'template'];
var ROTATE_FILTER_TYPE = 'ROTATE';

var ignoreNamespaceError = (ctx, fn) => {
  var {
    instances: {
      logger
    }
  } = ctx;
  return /*#__PURE__*/function () {
    var _ignoreNamespaceError = _asyncToGenerator(function* () {
      try {
        return yield fn(...arguments);
      } catch (err) {
        if (err.codeName === 'NamespaceNotFound') {
          logger.warn('getCurrentIndexes', err.errmsg);
        } else {
          throw err;
        }
      }
    });

    function ignoreNamespaceError() {
      return _ignoreNamespaceError.apply(this, arguments);
    }

    return ignoreNamespaceError;
  }();
};

var getCurrentIndexes = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, dbModel, db) {
    var arrOfIndexes = yield _bluebird.default.map(_lodash.default.keys(dbModel), ignoreNamespaceError(ctx, /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator(function* (configModel) {
        var indexes = yield db.model(configModel).indexes();
        return {
          name: dbModel[configModel].name,
          indexes: _lodash.default.filter(indexes || [], index => index.name !== '_id_')
        };
      });

      return function (_x4) {
        return _ref2.apply(this, arguments);
      };
    }()));
    return _lodash.default.compact(arrOfIndexes);
  });

  return function getCurrentIndexes(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

var reIndex = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator(function* (ctx, dbs, dbName, options) {
    var {
      instances: {
        logger
      }
    } = ctx;

    var isForced = _lodash.default.get(options, 'isForced', true);

    var filterModelType = _lodash.default.get(options, 'filter.modelType');

    var isFilterTypeRotate = filterModelType === ROTATE_FILTER_TYPE;
    var db = dbs[dbName];

    var dbModels = require(_path.default.join(global.MAIN_DIR, 'models')).default;

    var dbModel = _lodash.default.pickBy(dbModels[dbName], model => {
      var isRotateType = _lodash.default.includes(ROTATE_MODEL_TYPES, _lodash.default.get(model, 'suffix.type'));

      return isRotateType === isFilterTypeRotate;
    });

    if (!dbModel || !db) {
      throw new Error("Could not find DB ".concat(dbName));
    }

    logger.info("Working on [".concat(dbName, "]..."));

    try {
      var dbStats = yield getCurrentIndexes(ctx, dbModel, db);

      var modelIndexes = _lodash.default.map(dbModel, (model, name) => ({
        name: name,
        collection: model.name,
        indexes: model.indexes || []
      }));

      var results = yield _bluebird.default.each(modelIndexes, /*#__PURE__*/function () {
        var _ref4 = _asyncToGenerator(function* (model) {
          logger.info("-> [".concat(model.name, "]:[").concat(model.collection, "] [type:").concat(filterModelType, "]"));

          var stats = _lodash.default.find(dbStats, collection => collection.name === model.collection);

          var dbIndexes = _lodash.default.get(stats, 'indexes', []);

          var updatingIndexes = _lodash.default.differenceBy(model.indexes, dbIndexes, index => {
            var listProps = ['name', 'key', 'unique', 'expireAfterSeconds', 'min', 'max', 'partialFilterExpression'];
            return JSON.stringify(_lodash.default.pick(index, listProps));
          });

          var redundantIndexes = _lodash.default.differenceBy(dbIndexes, model.indexes, 'name');

          var collection = db.model(model.name);
          yield _bluebird.default.each(redundantIndexes, /*#__PURE__*/function () {
            var _ref6 = _asyncToGenerator(function* (_ref5) {
              var {
                name
              } = _ref5;

              try {
                yield ignoreNamespaceError(ctx, /*#__PURE__*/_asyncToGenerator(function* () {
                  yield collection.dropIndex(name);
                }))();
                logger.info("    - [".concat(name, "]"));
              } catch (err) {
                if (err.codeName === 'IndexNotFound') {
                  return;
                }

                throw err;
              }
            });

            return function (_x10) {
              return _ref6.apply(this, arguments);
            };
          }());
          yield _bluebird.default.each(updatingIndexes, /*#__PURE__*/function () {
            var _ref9 = _asyncToGenerator(function* (_ref8) {
              var {
                key,
                name
              } = _ref8,
                  options = _objectWithoutProperties(_ref8, _excluded);

              yield ignoreNamespaceError(ctx, /*#__PURE__*/_asyncToGenerator(function* () {
                var isIndexExisted = yield collection.indexExists(name);

                if (isForced && isIndexExisted) {
                  yield collection.dropIndex(name);
                  logger.info("    - [".concat(name, "]"));
                }
              }))();
              yield collection.createIndex(key, _objectSpread(_objectSpread({}, options), {}, {
                name
              }));
              logger.info("    + [".concat(collection.name, "]:[").concat(name, "]"));
            });

            return function (_x11) {
              return _ref9.apply(this, arguments);
            };
          }());
        });

        return function (_x9) {
          return _ref4.apply(this, arguments);
        };
      }());

      if (results.length) {
        logger.info('MongoDB indexed successfully');
      } else {
        logger.info('MongoDB indexes up-to-date');
      }
    } catch (err) {
      logger.error('MongoDB indexed failed', err);
    }
  });

  return function reIndex(_x5, _x6, _x7, _x8) {
    return _ref3.apply(this, arguments);
  };
}();

exports.reIndex = reIndex;