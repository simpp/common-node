"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.seed = void 0;

var _path = _interopRequireDefault(require("path"));

var _lodash = _interopRequireDefault(require("lodash"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _excluded = ["_id"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var seed = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, dbs, dbName) {
    var {
      instances: {
        logger
      }
    } = ctx;
    var {
      default: dbSeeds
    } = yield Promise.resolve("".concat(_path.default.join(global.MAIN_DIR, 'models/seed'))).then(s => _interopRequireWildcard(require(s)));
    var db = dbs[dbName];
    var seeds = _lodash.default.values(dbSeeds[dbName]) || [];

    if (!db) {
      throw new Error("No db for [".concat(dbName, "]"));
    }

    logger.info("Starting...");
    yield _bluebird.default.each(seeds, /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator(function* (seed) {
        if (!seed.model) {
          logger.info("seeding [".concat(dbName, "], no model found"), seed);
          return;
        }

        var collection = db.use(seed.model.name);
        logger.info("seeding [".concat(seed.model.name, "]"));

        if (!collection) {
          logger.error("seeding on [".concat(seed.model.name, "], no collection found"));
          return;
        }

        var duplidated = _lodash.default.difference(seed.data, _lodash.default.uniqBy(seed.data, '_id'));

        if (!_lodash.default.isEmpty(duplidated)) {
          logger.error("[".concat(seed.model.name, "] has duplidated"), duplidated);
          return;
        }

        if (seed.before) {
          yield seed.before(ctx, {
            seed,
            db,
            collection
          });
        }

        yield _bluebird.default.mapSeries(seed.data, /*#__PURE__*/function () {
          var _ref3 = _asyncToGenerator(function* (row) {
            logger.debug('seeding...');
            logger.debug(row);
            var executor = seed.executor;
            var builder = seed.builder;
            var transform = seed.transform;

            if (executor) {
              if (builder) {
                logger.warn('[executor] is provided, ignore [builder]');
              }

              if (transform) {
                logger.warn('[executor] is provided, ignore [transform]');
              }

              yield executor(ctx, {
                seed,
                db,
                collection,
                row
              });
              logger.debug('done by executor...');
              return;
            }

            if (builder) {
              if (transform) {
                logger.warn('[builder] is provided, ignore [transform]');
              }

              var params = yield builder(ctx, {
                seed,
                db,
                collection,
                row
              });
              yield collection.updateOne(...params);
              logger.debug('done by builder...');
              return;
            }

            var _ref4 = transform ? yield transform(ctx, {
              seed,
              db,
              collection,
              row
            }) : row,
                {
              _id: rowId
            } = _ref4,
                rowData = _objectWithoutProperties(_ref4, _excluded);

            yield collection.updateOne({
              _id: db.toId(rowId)
            }, {
              $setOnInsert: {
                _id: db.toId(rowId),
                createdAt: Date.now()
              },
              $set: _objectSpread(_objectSpread({}, collection.of(rowData)), {}, {
                updatedAt: Date.now()
              })
            }, {
              upsert: true
            });
            logger.debug('done...');
          });

          return function (_x5) {
            return _ref3.apply(this, arguments);
          };
        }());

        if (seed.after) {
          yield seed.after(ctx, {
            seed,
            db,
            collection
          });
        }
      });

      return function (_x4) {
        return _ref2.apply(this, arguments);
      };
    }());
    logger.info("seeding on [".concat(dbName, "] is done"));
  });

  return function seed(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.seed = seed;