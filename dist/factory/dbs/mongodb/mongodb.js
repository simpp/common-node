"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toId = exports.init = exports.createConnection = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _bluebird = _interopRequireDefault(require("bluebird"));

var _mongodb = require("mongodb");

var _config = require("../../../libs/config");

var Verb = _interopRequireWildcard(require("./verb"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _eventHandlers = {
  onConnected: _lodash.default.stubTrue,
  onConnectError: _lodash.default.stubTrue,
  onClosed: _lodash.default.stubTrue,
  onPingFailed: _lodash.default.stubTrue,
  onPingSucceed: _lodash.default.stubTrue,
  onPingError: _lodash.default.stubTrue
};

var listeners = logger => ({
  onConnected: dbName => {
    logger.info("Mongodb [".concat(dbName, "] connected..."));

    _eventHandlers.onConnected(dbName);
  },
  onConnectError: (dbName, err) => {
    logger.error("Mongodb [".concat(dbName, "] error..."), err);

    _eventHandlers.onConnectError(dbName, err);
  },
  onClosed: dbName => {
    logger.warn("Mongodb [".concat(dbName, "] closed..."));

    _eventHandlers.onClosed(dbName);
  },
  onPingFailed: dbName => {
    logger.debug("Mongodb [".concat(dbName, "] ping failed"));

    _eventHandlers.onPingFailed(dbName);
  },
  onPingSucceed: dbName => {
    logger.debug("Mongodb [".concat(dbName, "] ping success"));

    _eventHandlers.onPingSucceed(dbName);
  },
  onPingError: dbName => {
    logger.warn("Mongodb [".concat(dbName, "] ping reaching threshold"));

    _eventHandlers.onPingError(dbName);
  }
});

var selectCollection = (db, name, prefix) => {
  return db.collection("".concat(prefix || '').concat(name));
};

var getModel = (db, dbModel, dbPrefix) => {
  return function getModel(name, data) {
    var model = dbModel[name];

    if (!model) {
      throw new Error("missing model [".concat(name, "]"));
    }

    var type = _lodash.default.get(model, ['suffix', 'type']);

    var format = _lodash.default.get(model, ['suffix', 'format'], '');

    var collection;

    if (type === 'moment') {
      var formatted = format ? _moment.default.utc(data).format(format) : '';
      collection = selectCollection(db, "".concat(model.name).concat(formatted), dbPrefix);
    } else if (type === 'template') {
      var _formatted = _lodash.default.template(format)(data);

      collection = selectCollection(db, "".concat(model.name).concat(_formatted), dbPrefix);
    } else {
      collection = selectCollection(db, "".concat(model.name).concat(format), dbPrefix);
    }

    return Verb.bind(collection, model);
  };
};

var useCollection = (conn, dbModel, dbPrefix) => {
  var modelByName = getModel(conn, dbModel, dbPrefix);
  return function useCollection(collection) {
    var model = _lodash.default.findKey(dbModel, ['name', collection]);

    if (!model) {
      throw new Error("missing collection ".concat(collection));
    }

    return modelByName(model);
  };
};

var startPingStrategy = (name, client, db, config, logger) => {
  var interval = Math.max(_lodash.default.get(config, 'interval', 5000), 5000);
  var threshold = Math.max(_lodash.default.get(config, 'failureThreshold', 0), 6);
  var isWaiting = false;
  var counter = 0;
  setInterval(() => {
    if (isWaiting) {
      return;
    }

    isWaiting = true;
    db.admin().ping((err, result) => {
      var success = _lodash.default.get(result, 'ok');

      if (err) {
        logger.warn(name, 'ping', err);
      }

      if (success && counter) {
        counter = 0;
        listeners(logger).onPingSucceed(name);
      }

      if (!success) {
        listeners(logger).onPingFailed(name);
        counter++;
      }

      if (counter >= threshold) {
        counter = 0;
        listeners(logger).onPingError(name);
      }

      isWaiting = false;
    });
  }, interval);
};

var createConnection = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (name, config, logger) {
    var {
      uri,
      hosts,
      host,
      port,
      db,
      options,
      ping
    } = yield (0, _config.parse)(config);
    var mongoUri = uri || "mongodb://".concat(hosts || _lodash.default.join([host, port], ':'));
    logger.info("Mongodb connecting to [".concat(mongoUri, "]"));
    var dbInstance;
    var mongoClient;

    try {
      mongoClient = yield _mongodb.MongoClient.connect(mongoUri, _objectSpread(_objectSpread({}, options), {}, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }));
      dbInstance = mongoClient.db(db);

      if (ping) {
        startPingStrategy(name, mongoClient, dbInstance, ping, logger);
      }

      listeners(logger).onConnected(name, dbInstance);
    } catch (err) {
      dbInstance = null;
      listeners(logger).onConnectError(name, err);
      throw new Error(err);
    }

    logger.info("Mongodb connecting to [".concat(mongoUri, "], success"));
    return {
      db: dbInstance,
      client: mongoClient
    };
  });

  return function createConnection(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.createConnection = createConnection;

var init = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (ctx, dbConfig, dbModels, eventHandlers) {
    var {
      instances: {
        logger
      }
    } = ctx;
    _eventHandlers = _objectSpread(_objectSpread({}, _eventHandlers), eventHandlers);

    var configNames = _lodash.default.compact(_lodash.default.map(dbConfig, (config, configName) => {
      if (config.type === 'mongodb') {
        return configName;
      }
    }));

    var connections = {};
    yield _bluebird.default.each(configNames, /*#__PURE__*/function () {
      var _ref3 = _asyncToGenerator(function* (configName) {
        connections[configName] = yield createConnection(configName, dbConfig[configName], logger);
      });

      return function (_x8) {
        return _ref3.apply(this, arguments);
      };
    }());

    var mongodbByModels = _lodash.default.pickBy(dbModels, (dbModel, configName) => _lodash.default.includes(configNames, configName));

    return {
      connections,
      configNames,
      mongoDbByModels: bindMongoUtils(mongodbByModels, connections, dbConfig)
    };
  });

  return function init(_x4, _x5, _x6, _x7) {
    return _ref2.apply(this, arguments);
  };
}();

exports.init = init;

var toId = (idObj, increment) => {
  var objectId = new _mongodb.ObjectId(idObj);

  if (!increment) {
    return objectId;
  }

  var hexaTime = (+objectId.getTimestamp() + increment * 1000).toString(16).substring(0, 8);
  return new _mongodb.ObjectId(_lodash.default.replace(idObj, /^([0-9a-f]{8})(.+)$/, hexaTime + '$2'));
};

exports.toId = toId;

var execWithTransaction = conn => {
  var client = conn.client;
  return /*#__PURE__*/function () {
    var _ref4 = _asyncToGenerator(function* (fn, options) {
      var session = client.startSession();

      try {
        yield session.withTransaction(() => fn(session), {
          readPreference: _lodash.default.get(options, 'readPreference', 'primary'),
          readConcern: _lodash.default.get(options, 'readConcern', {
            level: 'local'
          }),
          writeConcern: _lodash.default.get(options, 'writeConcern', {
            w: 'majority'
          })
        });
        yield session.endSession();
      } catch (err) {
        yield session.endSession();
        throw err;
      }
    });

    return function (_x9, _x10) {
      return _ref4.apply(this, arguments);
    };
  }();
};

var bindMongoUtils = (mongodbByModels, connections, dbConfigs) => {
  var mongoDbModels = _lodash.default.mapValues(mongodbByModels, (dbModel, configName) => {
    var conn = connections[configName];

    var prefix = _lodash.default.get(dbConfigs, [configName, 'prefix'], '');

    var _models = _lodash.default.keys(dbModel);

    var modelByName = getModel(conn.db, dbModel, prefix);
    var useBycollection = useCollection(conn.db, dbModel, prefix);
    var boundModel = {
      model: modelByName,
      models: model => model ? modelByName(model) : _models,
      use: useBycollection,
      get: collection => selectCollection(conn.db, collection, prefix),
      toId,
      toIds: ids => _lodash.default.map(_lodash.default.castArray(ids), id => toId(id)),
      withSession: fn => conn.client.withSession(fn),
      startSession: () => conn.client.startSession(),
      withTransaction: execWithTransaction(conn),
      conn
    };

    _lodash.default.each(dbModel, (model, name) => {
      Object.defineProperty(boundModel, _lodash.default.upperFirst(_lodash.default.camelCase(name)), {
        get: () => modelByName(name)
      });
    });

    return Object.freeze(boundModel);
  });

  return mongoDbModels;
};