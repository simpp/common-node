"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.invoke = exports.init = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _http = require("../../libs/http");

var _querystring = _interopRequireDefault(require("querystring"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var init = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator(function* (ctx, config) {
    var client = (0, _http.httpCreator)(config);
    return {
      telegram: {
        client,
        config
      }
    };
  });

  return function init(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.init = init;

var invoke = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator(function* (ctx, config, telegram, fn, data) {
    var {
      instances: {
        logger
      }
    } = ctx;
    var url = "/bot".concat(config.token, "/").concat(fn);
    var result = yield telegram.client.post(url, {
      data: _querystring.default.stringify(data),
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      }
    });

    var isOk = _lodash.default.get(result, 'data.ok');

    if (isOk) {
      return result.data;
    }

    logger.error('failed invoke telegram', fn, data, result);
    return result.data;
  });

  return function invoke(_x3, _x4, _x5, _x6, _x7) {
    return _ref2.apply(this, arguments);
  };
}();

exports.invoke = invoke;