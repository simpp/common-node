"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppEnv = void 0;

var _keymirror = _interopRequireDefault(require("keymirror"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AppEnv = (0, _keymirror.default)({
  local: null,
  staging: null,
  prod: null
});
exports.AppEnv = AppEnv;