import keyMirror from 'keymirror'

const AppEnv = keyMirror({
  local: null,
  staging: null,
  prod: null,
})

export {AppEnv}
