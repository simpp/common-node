import _ from 'lodash'
import Bluebird from 'bluebird'
import {read} from './secret'
import {readJsonAsync} from './file'

const format = (data, formatters) => {
  return _.reduce(
    formatters,
    (result, formatter) => eval(formatter)(result),
    data
  )
}

const parseKeyPath = async (configKeyPath, configDefault, nodePipe) => {
  const valueFromFile = await read(configKeyPath)

  return format(valueFromFile, nodePipe)
}
const parseEnvVar = async (configEnvVar, configDefault, nodePipe) => {
  const valueFromEnv = _.get(process.env, configEnvVar, configDefault)

  return format(valueFromEnv, nodePipe)
}
const parseJsonPath = async (
  configJsonPath,
  configJsonProp,
  configDefault,
  nodePipe
) => {
  const jsonFromFile = await readJsonAsync(configJsonPath)
  const valueFromJson = configJsonProp
    ? _.get(jsonFromFile, configJsonProp, configDefault)
    : jsonFromFile

  return format(valueFromJson, nodePipe)
}

const parseRoot = async (config) => {
  if (typeof config !== 'object') {
    return config
  }

  const configKeyPath = _.get(config, ['__parse+keyPath'])
  const configJsonPath = _.get(config, ['__parse+jsonPath'])
  const configJsonProp = _.get(config, ['__parse+jsonProp'])
  const configEnvVar = _.get(config, ['__parse+envVar'])
  const configDefault = _.get(config, ['__parse+default'])

  const nodePipe = _.castArray(_.get(config, ['__parse+nodePipe']) || [])

  if (typeof configEnvVar === 'string') {
    return await parseEnvVar(configEnvVar, configDefault, nodePipe)
  } else if (typeof configKeyPath === 'string') {
    return await parseKeyPath(configKeyPath, configDefault, nodePipe)
  } else if (typeof configJsonPath === 'string') {
    return await parseJsonPath(
      configJsonPath,
      configJsonProp,
      configDefault,
      nodePipe
    )
  }

  return config
}

export const parse = async (input) => {
  const config = await parseRoot(input)

  if (typeof config !== 'object') {
    return config
  }

  const props = _.keys(config)

  const result = await Bluebird.reduce(
    props,
    async (parsedResult, prop) => {
      const configValue = _.get(config, prop)
      const configKeyPath = _.get(config, [prop, '__parse+keyPath'])
      const configJsonPath = _.get(config, [prop, '__parse+jsonPath'])
      const configJsonProp = _.get(config, [prop, '__parse+jsonProp'])
      const configEnvVar = _.get(config, [prop, '__parse+envVar'])
      const configDefault = _.get(config, [prop, '__parse+default'])

      const nodePipe = _.castArray(
        _.get(config, [prop, '__parse+nodePipe']) || []
      )

      parsedResult[prop] = configValue
      if (typeof configValue === 'string') {
        parsedResult[prop] = configValue
      } else if (typeof configEnvVar === 'string') {
        parsedResult[prop] = await parseEnvVar(
          configEnvVar,
          configDefault,
          nodePipe
        )
      } else if (typeof configKeyPath === 'string') {
        parsedResult[prop] = await parseKeyPath(
          configKeyPath,
          configDefault,
          nodePipe
        )
      } else if (typeof configJsonPath === 'string') {
        parsedResult[prop] = await parseJsonPath(
          configJsonPath,
          configJsonProp,
          configDefault,
          nodePipe
        )
      } else if (typeof configValue === 'object') {
        if (Array.isArray(configValue)) {
          parsedResult[prop] = await Bluebird.map(configValue, parse)
        } else {
          parsedResult[prop] = await parse(configValue)
        }
      } else {
        parsedResult[prop] = configValue
      }

      return parsedResult
    },
    {}
  )

  return result
}
