import _ from 'lodash'
import {Promise} from 'bluebird'

Promise.__proto__.allOf = async (arr, it) => {
  for (let i = 0; i < arr.length; i++) {
    let res = it
    if (_.isFunction(it)) {
      res = await it(arr[i])
    }

    if (!res) {
      return false
    }
  }

  return true
}

Promise.__proto__.anyOf = async (arr, it) => {
  for (let i = 0; i < arr.length; i++) {
    let res = it
    if (_.isFunction(it)) {
      res = await it(arr[i])
    }

    if (res) {
      return true
    }
  }

  return false
}

export default Promise
