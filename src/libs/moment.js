import moment from 'moment'

const ZERODATESTRING = '0001-01-01T00:00:00.000Z'

export const zeroMoment = (diff, unit = 'second') => {
  if (diff) {
    return moment.utc(ZERODATESTRING).add(diff, unit)
  }
  return moment.utc(ZERODATESTRING)
}

export const zeroDiff = (data, unit = 'second') => {
  return moment(data).diff(moment.utc(ZERODATESTRING), unit)
}

export const secOfWeek = (data) => {
  return moment(data).diff(moment.utc().startOf('week'), 'second')
}

export const secOfYear = (data) => {
  return moment(data).diff(moment.utc().startOf('year'), 'second')
}
