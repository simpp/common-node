import _ from 'lodash'
import {ObjectId} from 'mongodb'
import {validationResult, body, param, query} from 'express-validator'

import {ClientError} from './error'

export * from 'express-validator'

export const validate = (validators) => {
  return [
    ..._.castArray(validators),
    function validate(req, res, next) {
      const errorResult = validationResult(req).array()

      if (_.isEmpty(errorResult)) {
        return next()
      }

      const errorPair = _.map(errorResult, (error) => {
        const prop = _.get(error, 'param')
        const msg = _.get(error, 'msg')

        const first = _.get(msg, 'key', prop)
        const second = _.get(msg, 'error', msg)

        return [_.template(first)(error), _.template(second)(error)]
      })

      throw new ClientError(_.fromPairs(errorPair))
    },
  ]
}

export const bodyObjectId = (field, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return body(field).custom(function bodyObjectId(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    if (ObjectId.isValid(value)) {
      return true
    }

    throw new Error('[ObjectId].invalid')
  })
}

export const queryObjectId = (field, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return query(field).custom(function queryObjectId(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    if (ObjectId.isValid(value)) {
      return true
    }

    throw new Error('[ObjectId].invalid')
  })
}

export const paramObjectId = (field, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return param(field).custom(function paramObjectId(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    if (ObjectId.isValid(value)) {
      return true
    }

    throw new Error('[ObjectId].invalid')
  })
}

export const bodyArrayObjectIds = (field, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return body(field).custom(function bodyArrayObjectIds(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    const ids = _.castArray(value)
    if (_.every(ids, ObjectId.isValid)) {
      return true
    }

    throw new Error('[ObjectId].invalid')
  })
}

export const queryArrayObjectIds = (field, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return query(field).custom(function queryArrayObjectIds(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('[ObjectId].required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    const rawIds = _.isArray(value) ? value : _.split(value, ',')
    const ids = _.map(rawIds, (id) => _.trim(id, '"'))

    if (_.every(ids, (id) => ObjectId.isValid(id))) {
      return true
    }

    throw new Error('[ObjectId].invalid')
  })
}

export const queryInValues = (field, values, options) => {
  const ignoreIfEmpty = _.get(options, 'ignoreIfEmpty', true)
  const isRequired = _.get(options, 'isRequired', false)

  return query(field).custom(function queryObjectId(value) {
    if (_.isEmpty(value) && isRequired) {
      throw new Error('required')
    }

    if (_.isEmpty(value) && ignoreIfEmpty) {
      return true
    }

    if (_.includes(values, value)) {
      return true
    }

    throw new Error('invalid')
  })
}
