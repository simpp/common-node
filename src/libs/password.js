import _ from 'lodash'
import crypto from 'crypto'
import Bluebird from 'bluebird'

const asyncPbkdf2 = Bluebird.promisify(crypto.pbkdf2)

export const hash = async (
  password,
  salt,
  encoding = 'base64',
  digest = 'sha512'
) => {
  const hashedBuffer = await asyncPbkdf2(password, salt, 99139, 128, digest)

  return hashedBuffer.toString(encoding)
}

export const legacyHash = async (password, salt) => {
  const saltBuf = Buffer.from(salt, 'base64')
  const hashedBuffer = await asyncPbkdf2(password, saltBuf, 10000, 64, 'sha1')

  return hashedBuffer.toString('base64')
}

export const createSalt = (length = 128) => {
  const pattern =
    '1234567890!@#$%^&*()abcdefghiklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXYZ'

  return _.times(
    _.max([length, 32]),
    () => pattern[_.floor(Math.random() * pattern.length)]
  ).join('')
}
