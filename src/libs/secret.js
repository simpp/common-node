import crypto from 'crypto'
import _ from 'lodash'
import {readAsync} from './file'

const ALGORITHM = 'aes-256-cbc'
const KEY_LENGTH = 32
const IV_LENGTH = 16
const KEY_PATTERN =
  'abcdefghiklmnopqrstuvwxyz1234567890!@#$%^&*()ABCDEFGHIKLMNOPQRSTUVWXYZ`;,.[]{}-=+_?'
const IV_PATTERN =
  '1234567890abcdefghiklmnopqrstuvwxyz!@#$%^&*()`;,.[]{}-=+_?ABCDEFGHIKLMNOPQRSTUVWXYZ'

const toLength = (PATTERN, rawKey, LENGTH) => {
  let key = _.toString(rawKey).substring(0, LENGTH)
  let currentCount = 0

  for (let i = 0; i < key.length; i++) {
    currentCount += key.charCodeAt(i)
  }

  for (let i = key.length; i <= LENGTH; i++) {
    const newChar = PATTERN[currentCount % PATTERN.length]
    key += newChar
    currentCount += newChar.charCodeAt(0) * currentCount
  }

  return Buffer.from(key).toString('base64').substring(0, LENGTH)
}

export const encryptJSON = (jsonData = {}, key, iv) => {
  try {
    const cipher = crypto.createCipheriv(
      ALGORITHM,
      Buffer.from(toLength(key, KEY_PATTERN, KEY_LENGTH)),
      Buffer.from(toLength(iv || key, IV_PATTERN, IV_LENGTH))
    )

    const encryptedData = Buffer.concat([
      cipher.update(JSON.stringify(jsonData)),
      cipher.final(),
    ]).toString('hex')

    return _.map(
      _.chunk(encryptedData, Math.ceil(Math.sqrt(encryptedData.length))),
      (str) => _.join(str, '')
    )
  } catch (error) {
    throw error
  }
}

export const decryptJSON = (encryptedData, key, iv = '') => {
  try {
    const decipher = crypto.createDecipheriv(
      ALGORITHM,
      Buffer.from(toLength(key, KEY_PATTERN, KEY_LENGTH)),
      Buffer.from(toLength(iv || key, IV_PATTERN, IV_LENGTH))
    )

    const decrypted = Buffer.concat([
      decipher.update(_.join(encryptedData, ''), 'hex'),
      decipher.final(),
    ]).toString()

    return JSON.parse(decrypted)
  } catch (error) {
    throw error
  }
}

export const read = async (file) => {
  const content = await readAsync(file)

  return _.trim(content, '\n')
}
