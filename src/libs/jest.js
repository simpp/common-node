import _ from 'lodash'

export const jestContext = (ctx = {}) => {
  const context = _.cloneDeep(ctx)
  context.__proto__.mockResolvedValue = function (path, value) {
    const fn = jest.fn().mockResolvedValue(value)
    _.set(this, path, fn)

    return fn
  }
  context.__proto__.mockRejectedValue = function (path, value) {
    const fn = jest.fn().mockRejectedValue(value)
    _.set(this, path, fn)

    return fn
  }

  context.__proto__.mockReturnValue = function (path, value) {
    const fn = jest.fn().mockReturnValue(value)
    _.set(this, path, fn)

    return fn
  }

  context.__proto__.mockImplementation = function (path, ...args) {
    const fn = jest.fn().mockImplementation(...args)
    _.set(this, path, fn)

    return fn
  }

  return context
}
