import _ from 'lodash'

_.__proto__.isSEqual = (a, b) => _.toString(a) === _.toString(b)

export default _
