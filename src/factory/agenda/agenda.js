import Agenda from 'agenda'
import Agendash from 'agendash'

import Mongodb from '@/factory/dbs/mongodb'

const init = async (ctx, config = {}) => {
  const {
    instances: {logger},
  } = ctx

  const {database, options} = config
  const {db: mongoInstance} = await Mongodb.createConnection(
    'agenda',
    database,
    logger
  )

  const result = await fromMongodb(ctx, mongoInstance, options)

  return result
}

const fromMongodb = async (ctx, mongoInstance, options) => {
  const agenda = new Agenda({
    ...options,
    mongo: mongoInstance,
  })

  const agendash = Agendash(agenda)

  return {agenda, agendash}
}

export {init, fromMongodb}
