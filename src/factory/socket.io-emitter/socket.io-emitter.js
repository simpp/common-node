import _ from 'lodash'
import {Emitter} from '@socket.io/redis-emitter'

export const init = async (ctx, options) => {
  const socketIOEmitter = new Emitter(options)

  return {socketIOEmitter}
}
