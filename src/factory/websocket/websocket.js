import _ from 'lodash'
import ltws from 'ltws'

export const init = (ctx, options) => {
  const websocket = ltws.connect(options.origin, options.config)

  return {websocket}
}

export const createInstance = () => {
  const websocket = ltws.createInstance()

  return {websocket}
}
