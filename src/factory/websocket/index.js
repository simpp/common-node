import {init, createInstance} from './websocket'

export default {init, createInstance}
