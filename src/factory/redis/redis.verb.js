import _ from 'lodash'
import {toKey} from './redis'

export const bind = (redis) => {
  const prefix = _.get(redis, 'options.keyPrefix', '')

  const verbs = []

  const redisLib = _.fromPairs(
    _.map(verbs, (verb) => [verb, redis[verb].bind(redis)])
  )

  redisLib.toKey = (paths) => `${prefix}${toKey(paths)}`
  redisLib.deKey = (key) => key.substr(_.size(prefix))

  return redisLib
}
