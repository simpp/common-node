import _ from 'lodash'
import RabbitMQ from 'amqplib'
import Bluebird from 'bluebird'

const defineQueue = (ctx, exchanges) => {
  const {
    instances: {logger},
  } = ctx

  const mappedQueue = _.flatMap(exchanges, (exchange) =>
    _.filter(
      _.castArray(_.get(exchange, 'queues', [])),
      (queue) => queue.consumer
    ).map((queue) => [queue.consumer.key, queue])
  )

  const consumerKeys = _.map(mappedQueue, 0)
  if (_.uniq(consumerKeys).length !== consumerKeys.length) {
    logger.error('[rabbitmq] consumer key must be uniq', consumerKeys)
    throw new Error('[rabbitmq] consumer key must be uniq')
  }

  return _.fromPairs(mappedQueue)
}

const defineExchange = (ctx, exchanges) => {
  const {
    instances: {logger},
  } = ctx

  const mappedExchange = _.filter(
    exchanges,
    (exchange) => exchange.publisher
  ).map((exchange) => [exchange.publisher.key, exchange])

  const publisherKeys = _.map(mappedExchange, 0)
  if (_.uniq(publisherKeys).length !== publisherKeys.length) {
    logger.error('[rabbitmq] consumer key must be uniq', publisherKeys)
    throw new Error('[rabbitmq] consumer key must be uniq')
  }

  return _.fromPairs(mappedExchange)
}

const createConsumer = (ctx, queueKey, channel) => {
  const {
    instances: {logger},
  } = ctx

  return function consumer(queue, handler, options) {
    const ackQueue =
      typeof queue === 'object' ? queue : _.find(queueKey, ['name', queue])

    const ackQueueOptions = _.get(ackQueue, 'consumer.options', {})
    const queueOptions = {
      ...ackQueueOptions,
      ...options,
    }
    const queueName = _.get(ackQueue, 'name', _.toString(queue))

    return channel.consume(
      queueName,
      async (message) => {
        try {
          const exchange = _.get(message, 'fields.exchange')
          const routingKey = _.get(message, 'fields.routingKey')
          const data = JSON.parse(message.content.toString())

          let isAck = _.get(queueOptions, 'noAck', false)
          const ackMessage = () => {
            if (!isAck) {
              isAck = true
              channel.ack(message)
            }
          }

          const result = await handler(data, {exchange, routingKey}, ackMessage)
          if (result && !isAck) {
            ackMessage()
          }
        } catch (err) {
          logger.error('[rabbitmq] consume error', err)
        }
      },
      queueOptions
    )
  }
}

const createPublisher = (ctx, exchangeKey, channel) => {
  const {
    instances: {logger},
  } = ctx

  return async function publisher(exchange, route, message, options) {
    const routingKey =
      typeof route === 'object' ? _.get(route, 'name', '') : route

    const exchageName =
      typeof exchange === 'object' ? _.get(exchange, 'name') : exchange

    const foundExchange = _.find(exchangeKey, ['name', exchageName])
    if (!foundExchange) {
      logger.error('[rabbitmq] exchange invalid', exchange)
      return
    }

    const defaultOptions = _.get(foundExchange, 'messageOptions', {})
    const messageContent = Buffer.from(
      typeof message === 'string' ? message : JSON.stringify(message)
    )
    channel.publish(foundExchange.name, routingKey, messageContent, {
      ...defaultOptions,
      ...options,
    })
  }
}

const bindRabbitUtils = (ctx, exchanges, channel) => {
  const queue = defineQueue(ctx, exchanges)
  const exchange = defineExchange(ctx, exchanges)

  const boundModel = {
    channel,
    queue,
    exchange,
    publish: createPublisher(ctx, exchange, channel),
    consume: createConsumer(ctx, queue, channel),
  }

  return boundModel
}

const init = async (ctx, rabbitmq) => {
  const {
    instances: {logger},
  } = ctx

  const connection = _.get(rabbitmq, 'connection')
  const exchanges = _.castArray(_.get(rabbitmq, 'exchanges', []))
  if (!connection) {
    logger.error('[rabbitmq] missing rabbitmq config')
    throw new Error('[rabbitmq] missing rabbitmq config')
  }

  const {hostname, vhost, port} = connection

  logger.info(`RabbitMQ connecting to [${hostname}:${port}][${vhost}]`)

  const conn = await RabbitMQ.connect(connection)
  const channel = await conn.createChannel()

  await Bluebird.each(exchanges, async (exchange) => {
    await channel.assertExchange(exchange.name, exchange.type, exchange.options)

    const queues = _.get(exchange, 'queues', [])
    await Bluebird.each(_.castArray(queues), async (queue) => {
      await channel.assertQueue(queue.name, queue.options)
      await channel.bindQueue(
        queue.name,
        exchange.name,
        _.isNil(queue.routingKey)
          ? queue.routingKey || queue.name
          : queue.routingKey
      )
    })
  })

  logger.info(`RabbitMQ connecting to [${hostname}:${port}][${vhost}], success`)

  return {rabbitmq: bindRabbitUtils(ctx, exchanges, channel)}
}

export {init}
