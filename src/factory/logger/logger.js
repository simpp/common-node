import util from 'util'
import _ from 'lodash'
import Bluebird from 'bluebird'
import Moment from 'moment'
import {createLogger, format} from 'winston'

import transport from './transport'

const transformData = (data) => {
  const formatters = _.map(data, (data) => {
    if (data instanceof Error) {
      return util.formatWithOptions(
        {colors: true, depth: 5, showHidden: true},
        '%O',
        data.stack
      )
    }

    switch (typeof data) {
      case 'string':
        return util.formatWithOptions({colors: true}, '%s', data)
      case 'number':
        return util.formatWithOptions({colors: true}, '%d', data)
      default:
        return util.formatWithOptions({colors: true, depth: 5}, '%O', data)
    }
  })

  return _.join(formatters, ' ')
}

const init = async (ctx) => {
  const {
    config: {
      logger: {transports, level},
    },
  } = ctx

  const {combine, printf, simple} = format

  const finalFormat = printf(
    ({message, level, [Symbol.for('splat')]: optionalData}) => {
      const data = _.filter(
        _.flatten([message, optionalData]),
        (item) => !_.isNil(item)
      )
      const transformedData = transformData(data)

      return `${Moment.utc().toISOString()} ${level} ${transformedData}`
    }
  )

  const defaultFormats = [simple(), finalFormat]

  const logger = createLogger({
    level,
    format: combine(...defaultFormats),
    transports: await Bluebird.map(transports, ({type, ...opts}) =>
      transport(type, opts, defaultFormats)
    ),
  })

  return {logger}
}

export {init}
