import _ from 'lodash'
import {Server} from 'socket.io'
import {createAdapter} from '@socket.io/redis-adapter'
import * as Verb from './socket.verb'

export const init = async (ctx, options) => {
  const {
    instances: {redis, server},
  } = ctx
  const socket = new Server(server, options)
  const {pubClient, subClient} = options

  socket.adapter(
    createAdapter(pubClient || redis, subClient || (redis && redis.duplicate()))
  )

  _.assign(socket, Verb.bind(socket), {
    toRoomId,
    toRoomIds,
  })

  return {socket}
}

export const toRoomId = (paths) => _.join(_.flattenDeep(paths), ':')
export const toRoomIds = (paths) => _.map(paths, toRoomId)
export const goToRooms = (socket, rooms) => {
  return _.reduce(rooms, (socket, room) => socket.to(toRoomId(room)), socket)
}
