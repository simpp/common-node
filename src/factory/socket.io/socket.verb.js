import _ from 'lodash'

import {toRoomId, goToRooms} from './socket.io'

export const bind = (socket) => {
  const verbs = []

  const socketLib = _.fromPairs(
    _.map(verbs, (verb) => [verb, socket[verb].bind(socket)])
  )

  socketLib.sendToRoom = (paths, data) => {
    return socket.to(toRoomId(paths)).send(data)
  }

  socketLib.sendToManyRoom = (paths, data) => {
    return goToRooms(socket, paths).send(data)
  }

  socketLib.emitTo = (paths, event, data) => {
    return socket.to(toRoomId(paths)).emit(event, data)
  }

  socketLib.emitToMany = (paths, event, data) => {
    return goToRooms(socket, paths).emit(event, data)
  }

  socketLib.with = (nsp) => bind(socket.of(nsp))

  socketLib.withMany = (...nsp) => {
    const nsps = _.flatten(nsp)

    return {
      sendToRoom: (...args) => {
        _.each(nsps, (nsp) => bind(socket.of(nsp)).sendToRoom(...args))
      },
      sendToManyRoom: (...args) => {
        _.each(nsps, (nsp) => bind(socket.of(nsp)).sendToManyRoom(...args))
      },
      emitTo: (...args) => {
        _.each(nsps, (nsp) => bind(socket.of(nsp)).emitTo(...args))
      },
      emitToMany: (...args) => {
        _.each(nsps, (nsp) => bind(socket.of(nsp)).emitToMany(...args))
      },
    }
  }

  return socketLib
}
