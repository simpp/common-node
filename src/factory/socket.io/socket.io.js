import _ from 'lodash'
import SocketIO from 'socket.io'
import * as Verb from './socket.verb'

export const init = async (ctx, options) => {
  const {
    instances: {server},
  } = ctx
  const socket = SocketIO(server, options)

  _.assign(socket, Verb.bind(socket), {
    toRoomId,
    goToRooms,
  })

  return {socket}
}

export const toRoomId = (paths) => _.join(_.flattenDeep(paths), ':')
export const toRoomIds = (paths) => _.map(paths, toRoomId)
export const goToRooms = (socket, rooms) => {
  return _.reduce(rooms, (socket, room) => socket.to(toRoomId(room)), socket)
}
