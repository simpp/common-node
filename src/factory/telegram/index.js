import * as Telegram from './telegram'
import {assignObjOnce} from '/libs/object'

const utilize = (ctx, config, telegram) => {
  assignObjOnce(telegram, {
    sendMessage: (data) =>
      Telegram.invoke(ctx, config, telegram, 'sendMessage', {
        chat_id: config.chatId,
        ...data,
      }),
    invoke: (data) => Telegram.invoke(ctx, config, telegram, data),
  })

  return telegram
}

const init = async (ctx, config) => {
  const {telegram} = await Telegram.init(ctx, config)

  return {telegram: utilize(ctx, config, telegram)}
}

export default {init}
