import _ from 'lodash'
import {httpCreator} from 'libs/http'
import queryString from 'querystring'

const init = async (ctx, config) => {
  const client = httpCreator(config)

  return {
    telegram: {client, config},
  }
}

const invoke = async (ctx, config, telegram, fn, data) => {
  const {
    instances: {logger},
  } = ctx

  const url = `/bot${config.token}/${fn}`
  const result = await telegram.client.post(url, {
    data: queryString.stringify(data),
    headers: {'content-type': 'application/x-www-form-urlencoded'},
  })

  const isOk = _.get(result, 'data.ok')
  if (isOk) {
    return result.data
  }

  logger.error('failed invoke telegram', fn, data, result)

  return result.data
}

export {init, invoke}
