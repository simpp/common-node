import _ from 'lodash'
import {
  httpCreator,
  proxyCreator,
  httpInternalFlow,
  httpFlow,
  httpInternalTransformers,
  httpTransformers,
} from '/libs/http'

const parseUrls = (origin) => {
  if (typeof origin === 'string' && _.includes(origin, ',')) {
    return _.split(origin, ',').filter((i) => i)
  }

  return _.castArray(origin || [])
}

const init = (ctx, serviceConfig) => {
  const createPool = (serviceConfig, name) => {
    const config = _.get(serviceConfig, name)
    if (!config) {
      throw new Error(`No service config found for target [${name}]`)
    }

    const pool = _.concat(
      _.map(parseUrls(config.baseUrl), (baseUrl) => ({
        ...config,
        baseUrl,
      })),
      _.map(parseUrls(config.origin), (origin) => ({
        ...config,
        origin,
      }))
    )

    if (_.isEmpty(pool)) {
      throw new Error(`Service config pool empty for target [${name}]`)
    }

    return pool
  }

  const locator = {
    locate: (name) => {
      const config = _.get(serviceConfig, name)
      if (!config) {
        throw new Error(`No service config found for target [${name}]`)
      }

      return httpCreator(config, {
        flowController: httpInternalFlow,
        transformers: httpInternalTransformers,
      })
    },
    nativeLocate: (name) => {
      const config = _.get(serviceConfig, name)
      if (!config) {
        throw new Error(`No service config found for target [${name}]`)
      }

      return httpCreator(config, {
        flowController: httpFlow,
        transformers: httpTransformers,
      })
    },
    proxyTo: (name) => {
      const pool = createPool(serviceConfig, name)

      return proxyCreator(pool, {
        flowController: httpInternalFlow,
        transformers: httpInternalTransformers,
      })
    },
  }

  return {locator}
}

const service = (ctx, serviceConfig) => {
  const locator = {
    locate: (() => {
      const config = serviceConfig
      if (!config) {
        throw new Error(`No service config found for target`)
      }

      return httpCreator(config, {
        flowController: httpInternalFlow,
        transformers: httpInternalTransformers,
      })
    })(),
    nativeLocate: (() => {
      const config = serviceConfig
      if (!config) {
        throw new Error(`No service config found for target`)
      }

      return httpCreator(config, {
        flowController: httpFlow,
        transformers: httpTransformers,
      })
    })(),
    proxyTo: (() => {
      const config = serviceConfig
      if (!config) {
        throw new Error(`No service config found for proxier`)
      }

      return proxyCreator(config, {
        flowController: httpInternalFlow,
        transformers: httpInternalTransformers,
      })
    })(),
  }

  return locator
}

export {init, service}
