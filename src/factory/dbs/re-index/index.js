import _ from 'lodash'
import path from 'path'
import {parse as parseConfig} from '/libs/config'
import {assignObjOnce} from '/libs/object'
import Databases from '/factory/dbs'
import Logger from '/factory/logger'

import {reIndex as mongodbReIndex} from '../mongodb/re-index'
import {confirmation} from '../confirmation'

const exec = async () => {
  const rawConfig = require(path.join(global.MAIN_DIR, 'config')).default
  const config = await parseConfig(rawConfig)

  const {logger} = await Logger.init({config})

  const ctx = assignObjOnce(
    {},
    {
      config,
      instances: {logger},
    }
  )

  const dbConfig = ctx.config.databases
  const databases = await Databases.init(ctx, dbConfig)
  assignObjOnce(ctx.instances, databases)

  const targetDb = process.env.SCRIPT_INDEX_DB_NAME || 'primary'
  const targetDbModelType = process.env.SCRIPT_INDEX_DB_MODEL_TYPE || 'default'
  if (!dbConfig[targetDb]) {
    throw new Error(`No config for [${targetDb}]`)
  }

  await confirmation(
    ctx,
    {
      name: 'RE-INDEX',
      warning: 'Press Control + C for cancelling',
      params: {targetDb, targetDbModelType},
    },
    5
  )

  if (dbConfig[targetDb].type === 'mongodb') {
    await mongodbReIndex(ctx, databases.dbs, targetDb, {
      filter: {
        modelType: targetDbModelType,
      },
    })
  }
}

export default exec().catch(console.error)
