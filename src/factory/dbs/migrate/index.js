import path from 'path'
import {parse as parseConfig} from '/libs/config'
import {assignObjOnce} from '/libs/object'
import Databases from '/factory/dbs'
import Logger from '/factory/logger'

import {migrate as mongodbMigrate} from '../mongodb/migrate'
import {confirmation} from '../confirmation'

const exec = async () => {
  const rawConfig = require(path.join(global.MAIN_DIR, 'config')).default
  const config = await parseConfig(rawConfig)

  const {logger} = await Logger.init({config})

  const ctx = assignObjOnce(
    {},
    {
      config,
      instances: {logger},
    }
  )

  const dbConfig = ctx.config.databases
  const databases = await Databases.init(ctx, dbConfig)
  assignObjOnce(ctx.instances, databases)

  const targetDb = process.env.SCRIPT_MIGRATE_DB_NAME || 'primary'
  const scriptName = process.env.SCRIPT_MIGRATE_NAME
  if (!dbConfig[targetDb]) {
    throw new Error(`No config for [${targetDb}]`)
  }

  await confirmation(ctx, {
    name: 'MIGRATION SCRIPT',
    warning: 'Press Control + C for cancelling',
    params: {targetDb, scriptName: scriptName || '<all>'},
  })

  if (dbConfig[targetDb].type === 'mongodb') {
    await mongodbMigrate(ctx, databases.dbs, targetDb, scriptName)
  }
}

export default exec().catch(console.error)
