import path from 'path'
import _ from 'lodash'
import Bluebird from 'bluebird'

export const seed = async (ctx, dbs, dbName) => {
  const {
    instances: {logger},
  } = ctx

  const {default: dbSeeds} = await import(
    path.join(global.MAIN_DIR, 'models/seed')
  )
  const db = dbs[dbName]
  const seeds = _.values(dbSeeds[dbName]) || []

  if (!db) {
    throw new Error(`No db for [${dbName}]`)
  }

  logger.info(`Starting...`)

  await Bluebird.each(seeds, async (seed) => {
    if (!seed.model) {
      logger.info(`seeding [${dbName}], no model found`, seed)

      return
    }

    const collection = db.use(seed.model.name)
    logger.info(`seeding [${seed.model.name}]`)
    if (!collection) {
      logger.error(`seeding on [${seed.model.name}], no collection found`)

      return
    }

    const duplidated = _.difference(seed.data, _.uniqBy(seed.data, '_id'))
    if (!_.isEmpty(duplidated)) {
      logger.error(`[${seed.model.name}] has duplidated`, duplidated)

      return
    }

    if (seed.before) {
      await seed.before(ctx, {seed, db, collection})
    }

    await Bluebird.mapSeries(seed.data, async (row) => {
      logger.debug('seeding...')
      logger.debug(row)

      const executor = seed.executor
      const builder = seed.builder
      const transform = seed.transform

      if (executor) {
        if (builder) {
          logger.warn('[executor] is provided, ignore [builder]')
        }
        if (transform) {
          logger.warn('[executor] is provided, ignore [transform]')
        }

        await executor(ctx, {seed, db, collection, row})

        logger.debug('done by executor...')

        return
      }

      if (builder) {
        if (transform) {
          logger.warn('[builder] is provided, ignore [transform]')
        }

        const params = await builder(ctx, {seed, db, collection, row})
        await collection.updateOne(...params)

        logger.debug('done by builder...')

        return
      }

      const {_id: rowId, ...rowData} = transform
        ? await transform(ctx, {seed, db, collection, row})
        : row

      await collection.updateOne(
        {_id: db.toId(rowId)},
        {
          $setOnInsert: {
            _id: db.toId(rowId),
            createdAt: Date.now(),
          },
          $set: {
            ...collection.of(rowData),
            updatedAt: Date.now(),
          },
        },
        {upsert: true}
      )

      logger.debug('done...')
    })

    if (seed.after) {
      await seed.after(ctx, {seed, db, collection})
    }
  })

  logger.info(`seeding on [${dbName}] is done`)
}
