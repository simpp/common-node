import path from 'path'
import _ from 'lodash'
import Bluebird from 'bluebird'

export const migrate = async (ctx, dbs, dbName, scriptName) => {
  const {
    instances: {logger},
  } = ctx

  const dbMigrations = require(path.join(
    global.MAIN_DIR,
    'models/migrate'
  )).default

  const db = dbs[dbName]
  const migrations = _.filter(
    _.flatMap(dbMigrations[dbName]),
    (script) => !scriptName || script.name === scriptName
  )

  if (!db) {
    throw new Error(`No db for [${dbName}]`)
  }

  logger.info(`Starting...`)

  await Bluebird.each(migrations, async (migration) => {
    logger.info(`Run migrate on [${migration.name}]...`)

    const migrationSteps = _.flatten(
      _.castArray(migration.steps || migration.step || [])
    )

    await Bluebird.reduce(
      migrationSteps,
      async ({results, prev}, step, stepIndex, stepCount) => {
        logger.info(`[${step.name || stepIndex + 1}] running...`)
        let curResult = null
        try {
          curResult = await step(ctx, {
            db,
            dbs,
            dbName,
            results,
            prev,
          })
        } catch (err) {
          logger.error(`[${step.name || stepIndex + 1}] failed!`, err)
          throw err
        }
        logger.info(
          `[${step.name || stepIndex + 1}] ${stepIndex + 1}/${stepCount} done.`
        )

        return {
          prev: curResult,
          results: [...results, curResult],
        }
      },
      {results: [], previous: null}
    )

    logger.info(`Run migrate on [${migration.name}], done.`)
  })

  logger.info(`migrate on [${dbName}] is done`)
}
