import path from 'path'
import _ from 'lodash'
import Bluebird from 'bluebird'

const ROTATE_MODEL_TYPES = ['moment', 'template']
const ROTATE_FILTER_TYPE = 'ROTATE'

const ignoreNamespaceError = (ctx, fn) => {
  const {
    instances: {logger},
  } = ctx

  return async function ignoreNamespaceError(...args) {
    try {
      return await fn(...args)
    } catch (err) {
      if (err.codeName === 'NamespaceNotFound') {
        logger.warn('getCurrentIndexes', err.errmsg)
      } else {
        throw err
      }
    }
  }
}

const getCurrentIndexes = async (ctx, dbModel, db) => {
  const arrOfIndexes = await Bluebird.map(
    _.keys(dbModel),
    ignoreNamespaceError(ctx, async (configModel) => {
      const indexes = await db.model(configModel).indexes()

      return {
        name: dbModel[configModel].name,
        indexes: _.filter(indexes || [], (index) => index.name !== '_id_'),
      }
    })
  )

  return _.compact(arrOfIndexes)
}

const reIndex = async (ctx, dbs, dbName, options) => {
  const {
    instances: {logger},
  } = ctx

  const isForced = _.get(options, 'isForced', true)
  const filterModelType = _.get(options, 'filter.modelType')
  const isFilterTypeRotate = filterModelType === ROTATE_FILTER_TYPE

  const db = dbs[dbName]
  const dbModels = require(path.join(global.MAIN_DIR, 'models')).default
  const dbModel = _.pickBy(dbModels[dbName], (model) => {
    const isRotateType = _.includes(
      ROTATE_MODEL_TYPES,
      _.get(model, 'suffix.type')
    )

    return isRotateType === isFilterTypeRotate
  })

  if (!dbModel || !db) {
    throw new Error(`Could not find DB ${dbName}`)
  }

  logger.info(`Working on [${dbName}]...`)
  try {
    const dbStats = await getCurrentIndexes(ctx, dbModel, db)
    const modelIndexes = _.map(dbModel, (model, name) => ({
      name: name,
      collection: model.name,
      indexes: model.indexes || [],
    }))

    const results = await Bluebird.each(modelIndexes, async (model) => {
      logger.info(
        `-> [${model.name}]:[${model.collection}] [type:${filterModelType}]`
      )
      const stats = _.find(
        dbStats,
        (collection) => collection.name === model.collection
      )
      const dbIndexes = _.get(stats, 'indexes', [])

      const updatingIndexes = _.differenceBy(
        model.indexes,
        dbIndexes,
        (index) => {
          const listProps = [
            'name',
            'key',
            'unique',
            'expireAfterSeconds',
            'min',
            'max',
            'partialFilterExpression',
          ]

          return JSON.stringify(_.pick(index, listProps))
        }
      )
      const redundantIndexes = _.differenceBy(dbIndexes, model.indexes, 'name')
      const collection = db.model(model.name)

      await Bluebird.each(redundantIndexes, async ({name}) => {
        try {
          await ignoreNamespaceError(ctx, async () => {
            await collection.dropIndex(name)
          })()
          logger.info(`    - [${name}]`)
        } catch (err) {
          if (err.codeName === 'IndexNotFound') {
            return
          }
          throw err
        }
      })

      await Bluebird.each(updatingIndexes, async ({key, name, ...options}) => {
        await ignoreNamespaceError(ctx, async () => {
          const isIndexExisted = await collection.indexExists(name)
          if (isForced && isIndexExisted) {
            await collection.dropIndex(name)
            logger.info(`    - [${name}]`)
          }
        })()
        await collection.createIndex(key, {...options, name})
        logger.info(`    + [${collection.name}]:[${name}]`)
      })
    })

    if (results.length) {
      logger.info('MongoDB indexed successfully')
    } else {
      logger.info('MongoDB indexes up-to-date')
    }
  } catch (err) {
    logger.error('MongoDB indexed failed', err)
  }
}

export {reIndex}
