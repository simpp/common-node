import {createConnection, init, toId} from './mongodb'

export default {createConnection, init, toId}
