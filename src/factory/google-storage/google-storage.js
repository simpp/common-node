import _ from 'lodash'
import {Storage} from '@google-cloud/storage'

const init = async (ctx, config) => {
  _.each(config.overwrite, (value, key) => {
    process.env[key] = value
  })

  const googleStorage = new Storage()

  return {googleStorage}
}

export {init}
