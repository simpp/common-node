import Nodemailer from 'nodemailer'

const init = async (ctx, options) => {
  const transporter = Nodemailer.createTransport(options)

  return transporter
}

export {init}
